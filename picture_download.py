#!/usr/bin/env python
import logging
import os.path
import sys
import requests
try:
	import configparser as configparser
except:
	import ConfigParser as configparser

import dudesnude
import dudesnude.maildir_sync


logging.basicConfig(level=logging.DEBUG)


# get the desired username
if len(sys.argv) < 2:
	print("You must specify a username to download pictures of")
	sys.exit(1)
other_username = sys.argv[1]
other_password = None
if len(sys.argv) == 3:
	other_password = sys.argv[2]

# load settings
try:
	_module_path = os.path.abspath(__file__)
	_cur_path = os.path.dirname(_module_path)
	_ini_path = os.path.join(_cur_path, 'settings.ini')
	config = configparser.RawConfigParser()
	config.read(_ini_path)
except Exception as e:
	print("Failure to load config: %s" % (e,))
	config = None

if config is not None and config.has_option('dudesnude', 'email'):
	# check for the user already being online
	if config.has_option('dudesnude', 'username'):
		username = config.get('dudesnude', 'username')
		guest = dudesnude.GuestClient()
		if guest.is_online(username):
			print("You appear to be logged in to the web interface, aborting")
			sys.exit(0)

	# log in
	dn_email = config.get('dudesnude', 'email')
	dn_password = config.get('dudesnude', 'password')
	client = dudesnude.Client(dn_email, dn_password)
else:
	client = dudesnude.GuestClient()

# look up the uid
uid = client.username2uid(other_username)
if uid is None:
	print("Could not locate user %s" % (other_username,))
	sys.exit(1)

# load message history pictures
history_pictures = []
if hasattr(client, 'get_message_history'):
	history = client.get_message_history(uid)
	received = [m for m in history if m['to'] == 'You']
	for m in received:
		history_pictures.extend([p['full'] for p in m.get('pictures', [])])
	if other_password is None:
		other_password = next((m['password'] for m in reversed(history) if m['to'] == 'You' and 'password' in m), None)

# load profile pictures
profile_pictures = []
profile_videos = []
profile = client.get_profile(uid, other_password)
profile_pictures = [p['full'] for p in profile['pictures']]
profile_videos = [v['full'] for v in profile['videos']]

# download
for url in profile_pictures + profile_videos + history_pictures:
	name = url.split('/')[-1]
	r = requests.get(url)
	with open(name, 'wb') as output:
		output.write(r.content)

# logout so the site sends normal email notifications
client.logout()
