from bs4 import BeautifulSoup, Tag
import dateutil.parser
import dateutil.tz
import re
from six.moves.urllib_parse import urljoin


BASE_URL = 'http://dudesnude.com/'


def message_list(html):
	re_email = re.compile(r'[\r\n]*My Email: (.*)')
	soup = BeautifulSoup(html, 'html5lib')
	soup_messages = None
	soup_tables = soup.find_all('table')
	for table in soup_tables:
		row = table.find('tr', class_='disp_row2')
		if row:
			soup_messages = table
	if soup_messages is None:
		return {'messages': [],
			'cur_page': 1,
			'num_pages': 1
		}
	soup_rows = soup_messages.find_all('tr')
	# collect all the messages on this view
	current_disp = ''
	messages = []
	message = {}
	for soup_row in soup_rows:
		klass = ' '.join(soup_row.attrs.get('class', []))
		if klass.startswith('disp_row') and \
		   current_disp != klass:
			current_disp = klass
			message = _message_firstrow(soup_row)
		elif klass.startswith('disp_row') and \
		     current_disp == klass:
			summary = soup_row.span.string
			found_email = re_email.search(summary)
			if found_email:
				summary = summary[:found_email.start()].rstrip()
			message['summary'] = summary
			messages.append(message)
	# figure out pagination
	soup_pages = soup_messages.find('td', {'align':'right'})
	soup_links = soup_pages.find_all('a')
	num_pages = len(soup_links) + 1
	other_pages = [s.string for s in soup_links]
	cur_page = 1
	for i in range(0, len(other_pages)):
		if other_pages[i] != str(i+1):
			cur_page = i + 1
			break
	return {'messages': messages,
	        'cur_page': cur_page,
	        'num_pages': num_pages
	}

def _message_firstrow(soup_row):
	message = {}
	soup_cells = soup_row.find_all('td', recursive=False)
	soup_profile = soup_cells[0]
	soup_message = soup_cells[1]
	soup_dudelist = soup_cells[2]
	soup_timestamp = soup_cells[3]
	icon = soup_message.a.img.attrs['src'].split('/')[-1]
	icon_to_state = {
		'Communication-53.png': 'unread',
		'Communication-54.png': 'read',
		'Communication-70.png': 'new',
		'Communication-74.png': 'replied',
		'Edition-44.png': 'deleted'
	}
	message['username'] = soup_profile.contents[0].string.strip()
	message['userid'] = soup_dudelist.a.attrs['href'].split('=')[1].split('&')[0]
	message['messageid'] = soup_message.a.attrs['href'].split('=')[1].split('&')[0]
	message['timestamp'] = dateutil.parser.parse(soup_timestamp.string)
	message['timestamp'] = message['timestamp'].replace(tzinfo=dateutil.tz.tzutc())
	message['state'] = icon_to_state[icon]
	return message

def message_history(html):
	re_email = re.compile(r'^My Email: (.*)$')
	re_password = re.compile(r'^private password attached: (.*)$')
	re_partner = re.compile(r'exchanged the following messages with (.*) - only messages')
	soup = BeautifulSoup(html, 'html5lib')
	soup_messages = soup.find('td')
	if soup_messages is None:
		return []
	soup_intro = soup_messages.find_previous('p')
	partner_match = re_partner.search(soup_intro.string)
	partner = partner_match.group(1)
	messages = []
	message = None
	for soup_p in soup_messages.find_all('p', recursive=False):
		if soup_p.i:	# first <p> of message
			if message:
				messages.append(message)
			message = {}
			message['timestamp'] = dateutil.parser.parse(soup_p.i.get_text())
			message['timestamp'] = message['timestamp'].replace(tzinfo=dateutil.tz.tzutc())
			# get the text of the message
			soup_p.i.decompose()	# delete the timestamp from the text
			text = soup_p.get_text().lstrip()
			author,text = text.split(": ", 1)
			message['from'] = author
			if author == 'You':
				message['to'] = partner
			else:
				message['to'] = 'You'
			# take out any email found at the end
			lines = text.split('\n')
			email_match = re_email.match(lines[-1])
			if email_match:
				message['email'] = email_match.group(1)
				text = '\n'.join(lines[:-2])
			# now we have the entire message
			message['text'] = text
			continue
		if soup_p.get_text().strip().startswith('private password attached: '):
			password_match = re_password.match(soup_p.get_text().strip())
			message['password'] = password_match.group(1)
			continue
		if soup_p.span and soup_p.span.string.startswith('pictures sent with this message'):
			message['pictures'] = []
			for soup_a in soup_p.find_all('a'):
				picture = {}
				picture['preview'] = urljoin(BASE_URL, soup_a.img.attrs['src'])
				picture['full'] = picture['preview'].replace('/pic_t/', '/pic/')
				message['pictures'].append(picture)
	if message:
		messages.append(message)
	return messages


def search_results(html):
	results = []
	soup = BeautifulSoup(html, 'html5lib')
	soup_main_content = soup.find(id='main_content')
	soup_table = soup_main_content.find('table')
	if soup_table is not None and soup_table.find_parent('div').attrs.get('align') == 'right':
		soup_table = soup_table.find_parent('div').find_next_sibling('table')
	if soup_table is None:
		# empty results
		return results
	soup_results = soup_table.find_next_sibling('table')
	for soup_row in soup_results.find_all('tr'):
		for soup_cell in soup_row.find_all('td'):
			if soup_cell.a:
				results.append(_result(soup_cell))
	return results

def _result(soup_cell):
	result = {}
	soup_img = soup_cell.a.img
	picture = {}
	picture['preview'] = urljoin(BASE_URL, soup_img.attrs['src'])
	picture['full'] = picture['preview'].replace('/pic_t/', '/pic/')
	result['picture'] = picture
	soup_username = soup_cell.a.find_next_sibling('a')
	userid = soup_username.attrs['href'].split('=')[-1]
	username = soup_username.string
	online = (soup_username.find_next_sibling('b', {'title':'dude online now'}) is not None)
	result['userid'] = userid
	result['username'] = username
	result['online'] = online
	return result

def profile(html):
	info = {}
	soup = BeautifulSoup(html, 'html5lib')
	soup_content = soup.find(id='main_content')
	soup_table = soup_content.find('table')
	# delete any add2any box
	soup_a2a = soup_table.find('div', class_='a2a_kit')
	if soup_a2a:
		soup_a2a_row = soup_a2a.find_parent('tr')
		soup_a2a_row.decompose()
	soup_rows = soup_table.tbody.find_all('tr', recursive=False)
	# parse name row
	soup_name = soup_rows[0].find('table')
	soup_name_rows = soup_name.tbody.find_all('tr', recursive=False)
	soup_name_real = soup_name_rows[0].td
	info['username'] = soup_name_real.span.string.split('@')[0].strip()
	info['online'] = soup_name_real.find('b', title='dude online now') is not None
	# parse age and location row
	info['verified'] = False
	info['supporter'] = False
	soup_age_row = soup_name_rows[2].find('tr')
	soup_ages = soup_age_row.td.find_all('td', class_='disp_row1')
	for soup_age in soup_ages:
		if soup_age.a and soup_age.a.string == 'VERIFIED':
			info['verified'] = True
		elif soup_age.a and soup_age.a['title'] == 'this guy supports dudesnude':
			info['supporter'] = True
		else:
			info['age'] = int(soup_age.string.split()[0])
	location_span = soup_age_row.find('span')
	if location_span:
		info['location'] = location_span.string.strip()
	# parse the info box
	info['infobox'] = {}
	soup_infobox = soup_rows[1].tr
	name = None
	value = None
	for soup_column in soup_infobox.find_all('td'):
		for item in soup_column.children:
			if isinstance(item, Tag):
				if item.name == 'b':
					name = item.string.strip(':')
			if not isinstance(item, Tag):
				value = item.string.strip()
			if isinstance(item, Tag) and name == 'links' and item.name == 'a':
				# parse link
				link = {}
				link['href'] = item['href']
				link['icon'] =  urljoin(BASE_URL, item.img.attrs['src'])
				link['name'] =  item.img.attrs['title']
				info.setdefault('links', []).append(link)
			if isinstance(item, Tag) and item.name == 'br':
				if name is not None and value is not None:
					info['infobox'][name] = value
				name = None
				value = None
		if name is not None and value is not None:
			info['infobox'][name] = value
	if 'profile id' in info['infobox']:
		info['userid'] = info['infobox']['profile id']
	if 'links' in info['infobox']:
		del info['infobox']['links']
	# parse the pictures
	info['pictures'] = []
	info['videos'] = []
	for soup_picture in soup_rows[3].find_all('a'):
		picture = {}
		picture['preview'] = urljoin(BASE_URL, soup_picture.img.attrs['src'])
		picture['full'] = picture['preview'].replace('/pic_t/', '/pic/')
		picture['private'] = False
		if soup_picture.img.get('onmouseover'):
			string = soup_picture.img['onmouseover']
			string = string.split('>', 1)[1]
			string = string.split('<', 1)[0]
			picture['comment'] = string
		info['pictures'].append(picture)
	# look for private pictures or videos
	soup_heading = soup_rows[3].find_next('td', class_='profile_pixlab')
	while soup_heading is not None:
		soup_section = soup_heading.find_parent('table').find_parent('tr').find_next_sibling('tr')
		if soup_heading.string == '::private pictures::':
			for soup_picture in soup_section.find_all('a'):
				picture = {}
				picture['preview'] = urljoin(BASE_URL, soup_picture.img.attrs['src'])
				picture['full'] = picture['preview'].replace('/pic_t/', '/pic/')
				picture['private'] = True
				if soup_picture.img.get('onmouseover'):
					string = soup_picture.img['onmouseover']
					string = string.split('>', 1)[1]
					string = string.split('<', 1)[0]
					picture['comment'] = string
				info['pictures'].append(picture)
		soup_heading = soup_heading.find_next('td', class_='profile_pixlab')
	for soup_video_link in soup_table.find_all('a', title='view this video clip'):
		video = {}
		soup_video_info = soup_video_link.find_parent('table')
		soup_video = soup_video_info.find_parent('table')
		soup_video_info_rows = soup_video_info.find_all('tr')
		video['private'] = soup_video_info_rows[0].find_all('td')[0].b is not None
		video['length'] = soup_video_info_rows[0].find_all('td')[-1].text
		video['size'] = soup_video_info_rows[2].find_all('td')[-1].text
		video_name = soup_video_link['href'].split('=')[1]
		video_subname = video_name.split('-')[1]
		video['view_link'] = urljoin(BASE_URL, soup_video_link['href'])
		video['download_link'] = urljoin(BASE_URL, soup_video_link.find_next_sibling('a')['href'])
		video['full'] = 'http://dudesnude.com/vid/%s/%s.mp4' % (video_name, video_subname)
		soup_video_previews = soup_video.find_all('img')
		video['previews'] = [img['src'] for img in soup_video_previews]
		info['videos'].append(video)
	# parse the profile text, which should just be text and <br>
	soup_profile_text = soup_rows[-1].td
	info['text'] = ''.join(soup_profile_text.strings).strip()
	return info
