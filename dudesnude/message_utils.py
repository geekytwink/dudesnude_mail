import datetime
import dudesnude
import email.encoders
import email.message
import email.mime.image
import email.mime.multipart
import email.mime.nonmultipart
import email.mime.text
import email.utils
import re
import requests
import six


class MIMETextUtf8(email.mime.text.MIMEText):
	def __init__(self, _text, _subtype='plain'):
		if isinstance(_text, six.text_type):
			_text = _text.encode('utf-8')
		sup = email.mime.text.MIMEText.__init__
		sup(self, _text, _subtype, _charset='utf-8')

		# utf8 encoding text, stolen from django
		utf8_charset = email.charset.Charset('utf-8')
		utf8_charset.body_encoding = None
		del self['Content-Transfer-Encoding']
		self.set_payload(_text, utf8_charset)


def _timestamp_unixtime(timestamp):
	unixtime = (timestamp - datetime.datetime(1970, 1, 1, tzinfo=timestamp.tzinfo)).total_seconds()
	return unixtime


def _tweak_message(message_summary, inbound=True):
	if inbound:
		message_summary['from'] = message_summary['username']
		message_summary['to'] = 'You'
	else:
		message_summary['from'] = 'You'
		message_summary['to'] = message_summary['username']


def _find_message(message_summary, message_history):
	for long_message in message_history:
		if long_message['timestamp'] == message_summary['timestamp'] and \
		   long_message['text'].startswith(message_summary['summary'][0:15]):
			return long_message
	return None


def _generate_messageid(message):
	timestamp = message['timestamp']
	unixtime = _timestamp_unixtime(timestamp)
	if 'messageid' in message:
		return '<%s:%s@dudesnude.com>' % (unixtime, message['messageid'])
	else:
		return '<%s:-@dudesnude.com>' % (unixtime, )

def convert_message(message):
	""" Converts a dudesnude message into an rfc2822 message """
	text_message = message['text']
	if 'password' in message:
		text_message = "%s\n\nPassword: %s" % (text_message, message['password'])
	if 'email' in message:
		text_message = "%s\n\nEmail: %s" % (text_message, message['email'])
	if len(message.get('pictures', [])) == 0:
		emessage = MIMETextUtf8(text_message)
	else:
		emessage = email.mime.multipart.MIMEMultipart()
		tmessage = MIMETextUtf8(text_message)
		emessage.attach(tmessage)
	for picture in message.get('pictures', []):
		pmessage = email.mime.image.MIMEImage(''.encode('utf-8'), _subtype='jpeg')
		pmessage['Content-Disposition'] = 'attachment; filename=%s; url=%s' % (
			picture['full'].split('/')[-1], picture['full']
		)
		del pmessage['Content-Transfer-Encoding']
		pmessage['Content-Transfer-Encoding'] = '7bit'
		pmessage['Content-Type'] = 'image/jpeg'
		emessage.attach(pmessage)
	# common bits in the main message
	emessage['Message-ID'] = _generate_messageid(message)
	emessage['Date'] = email.utils.formatdate(_timestamp_unixtime(message['timestamp']))
	emessage['From'] = '%s <%s@dudesnude.com>' % (message['from'], message['from'])
	emessage['To'] = '%s <%s@dudesnude.com>' % (message['to'], message['to'])
	return emessage


def _attach_picture(submessage):
	""" Replaces a single message part with the downloaded picture """
	url = None
	if submessage['Content-Type'] == 'image/jpeg':
		disp = submessage.get('Content-Disposition', '')
		found = re.search('url=([^;]*)', disp)
		if found:
			url = found.group(1)
	if url:
		r = requests.get(url)
		if r.status_code == 200:
			submessage.set_payload(r.content)
			del submessage['Content-Transfer-Encoding']
			email.encoders.encode_base64(submessage)


def attach_pictures(message):
	""" Looks through a multipart message for jpg attachments to download """
	if message.is_multipart():	# has attachments
		for part in message.get_payload():
			attach_pictures(part)
	else:
		# found a single part
		_attach_picture(message)


class Fetcher(object):
	def __init__(self, client):
		self.client = client
		# any message histories we've looked at
		self.histories = {}
		# any messages we've seen in inboxes, when look through history later
		self.seen_messages = {}	# {partner: {datetime: True}}

	def fetch(self, folder):
		""" Iterate through all of the messages in the given folder """
		MSGFOLDER = dudesnude.client.MSGFOLDER
		# prevent pagination message duplication
		box_messages = {}
		for message in self.client.iter_messages(folder=folder):
			userid = message['userid']
			if userid not in self.histories:
				history = self.client.get_message_history(userid)
				self.histories[userid] = history
			else:
				history = self.histories[userid]
			if box_messages.get(userid, {}).get(message['timestamp']):
				continue
			self.seen_messages.setdefault(userid, {})[message['timestamp']] = True
			box_messages.setdefault(userid, {})[message['timestamp']] = True
			_tweak_message(message, folder!=MSGFOLDER.SENT)
			long_message = _find_message(message, history)
			if long_message is not None:
				message.update(long_message)	# load attachments and full message
			yield message

	def fetch_remainder(self):
		""" Iterate through any history messages that haven't been seen in folders """
		for userid, history in self.histories.items():
			for message in history:
				date = message['timestamp']
				if not message['timestamp'] in self.seen_messages[userid]:
					message['state'] = 'read'
					yield message
