import logging
import mailbox
import dudesnude.client
import dudesnude.message_utils


logger = logging.getLogger(__name__)


def _message_equals(left, right):
	return left['To'] == right['To'] and \
	       left['From'] == right['From'] and \
	       left['Date'] == right['Date']

def _find_message(haystack, needle):
	if hasattr(haystack, 'iteritems') or hasattr(haystack, 'items'):
		items = haystack.iteritems if hasattr(haystack, 'iteritems') else haystack.items
		return next((item for item in items() if _message_equals(needle, item[1])), None)
	else:
		return next((item for item in haystack if _message_equals(needle, item)), None)


class MaildirMessages(object):
	def __init__(self):
		self.last_messages = []
		self.contents_by_person = {}
		self.unread = {}	# any messages that are unread

	def _message_person(self, message):
		username_from = message['From'].split('@')[0]
		username_to = message['To'].split('@')[0]
		username = username_from if username_from != 'You' else username_to
		return username

	def add(self, key, message):
		# get the conversation partner
		username_from = message['From'].split('@')[0]
		username_to = message['To'].split('@')[0]
		username = username_from if username_from != 'You' else username_to
		# save the message into the person history
		contents = self.contents_by_person.setdefault(username, [])
		contents.append(message)
		# update the most recent thing
		if len(self.last_messages) < 5 or \
		   self.last_messages[0]['Message-ID'] < message['Message-ID']:
			self.last_messages.append(message)
			self.last_messages.sort(key=lambda m:m['Message-ID'])
		# record any unread messages
		if 'S' not in message.get_flags():
			self.unread[key] = message

	def contains(self, message):
		# check for recent messages
		recent = _find_message(self.last_messages, message)
		if recent:
			return True
		# check entire history for person
		username = self._message_person(message)
		return _find_message(self.contents_by_person.get(username, []), message) is not None


class MaildirSync(object):
	sections = ['Inbox', 'Saved', 'Sent']
	def __init__(self, client, root, folder):
		self.client = client
		self.folder = folder
		self.mailbox = mailbox.Maildir(root, factory=None)
		if folder == '':
			self.mailbox_folder = self.mailbox
		else:
			folders = self.mailbox.list_folders()
			if folder not in folders:
				self.mailbox_folder = self.mailbox.add_folder(folder)
			else:
				self.mailbox_folder = self.mailbox.get_folder(folder)
		folders = self.mailbox_folder.list_folders()
		for section in self.sections:
			if section not in folders:
				self.mailbox_folder.add_folder(section)

	def get_mailbox(self, section):
		return self.mailbox_folder.get_folder(section)

	def get_mailbox_messages(self, section):
		mailbox = self.get_mailbox(section)
		messages = MaildirMessages()
		for key,message in mailbox.items():
			messages.add(key, message)
		return messages

	def sync(self):
		self.fetcher = dudesnude.message_utils.Fetcher(self.client)
		for section in self.sections:
			self._sync_section(section)
		self._sync_extra_sent()

	def _is_read(self, message):
		return message['state'] in ('read', 'replied')

	def _insert_message(self, target, message):
		rfc_message = dudesnude.message_utils.convert_message(message)
		dudesnude.message_utils.attach_pictures(rfc_message)
		full_message = mailbox.MaildirMessage(rfc_message)
		if self._is_read(message):
			full_message.add_flag('S')
		target.add(full_message)

	def _sync_section(self, section):
		target = self.get_mailbox(section)
		target.lock()
		try:
			downloading_new_messages = True
			current_messages = self.get_mailbox_messages(section)
			last_messages = current_messages.last_messages
			remaining_unread_messages = dict(current_messages.unread)
			folder_id = getattr(dudesnude.client.MSGFOLDER, section.upper())
			# download new messages
			for remote_message in self.fetcher.fetch(folder_id):
				logger.info("Processing message %s" % (remote_message,))
				if len(last_messages) == 0:
					logger.debug("Downloading everything for first run")
					# no messages downloaded, download everything
					self._insert_message(target, remote_message)
					continue
				rfc_message = dudesnude.message_utils.convert_message(remote_message)
				if current_messages.contains(rfc_message):
					# reached the end of the messages we
					# previously downloaded
					logger.debug("Found past message")
					downloading_new_messages = False
				if downloading_new_messages:
					logger.debug("New message")
					self._insert_message(target, remote_message)
					continue

				# update any previously-downloaded messages
				local_unread_message = _find_message(remaining_unread_messages, rfc_message)
				if local_unread_message:
					# a local unread message
					key = local_unread_message[0]
					if self._is_read(remote_message):
						# mark local message as read
						local_unread_message[1].add_flag('S')
						target[key] = local_unread_message[1]
					del remaining_unread_messages[key]
				# done refreshing any old messages
				if len(remaining_unread_messages) == 0:
					break
		finally:
			target.unlock()

	def _sync_extra_sent(self):
		target = self.get_mailbox('Sent')
		target.lock()
		try:
			current_messages = self.get_mailbox_messages('Sent')
			remaining_unread_messages = dict(current_messages.unread)
			for remote_message in self.fetcher.fetch_remainder():
				if remote_message['from'] != 'You':
					continue	# received but not in inbox?
				logger.debug("Looking to place extra sent message: %s"%(remote_message,))
				rfc_message = dudesnude.message_utils.convert_message(remote_message)
				# look for any unread messages that we should mark as read
				local_unread_message = _find_message(remaining_unread_messages, rfc_message)
				if local_unread_message:
					# a local unread message
					key = local_unread_message[0]
					if self._is_read(remote_message):
						# mark local message as read
						local_unread_message[1].add_flag('S')
						target[key] = local_unread_message[1]
					del remaining_unread_messages[key]
				# check to see if we have this message already
				if not current_messages.contains(rfc_message):
					self._insert_message(target, remote_message)
		finally:
			target.unlock()
