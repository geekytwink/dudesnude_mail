import requests
import re
import time

import dudesnude.parsers
from six.moves.http_cookiejar import CookieJar


class MSGFOLDER(object):
	INBOX = 1
	SAVED = 2
	SENT = 3
	DELETED = 4


class CommonClient(object):
	def username2uid(self, username):
		search_data = {
			'exact_name_match': '1',
			'name': username,
			'new_onlyf': '4',
			'open_newf': '1',
			'searching': '3',
			'stypef': '1'
		}
		r = self.session.post("https://dudesnude.com/runsearch.php", data=search_data)
		results = dudesnude.parsers.search_results(r.text)
		if len(results) > 0:
			return results[0]['userid']

	def is_online(self, username):
		search_data = {
			'exact_name_match': '1',
			'name': username,
			'new_onlyf': '4',
			'open_newf': '1',
			'searching': '3',
			'stypef': '1'
		}
		r = self.session.post("https://dudesnude.com/runsearch.php", data=search_data)
		results = dudesnude.parsers.search_results(r.text)
		if len(results) > 0:	# public profile
			return results[0]['online']

	def get_profile(self, uid, password=None):
		url = "https://dudesnude.com/show.php?id=%s" % (uid,)
		if password is not None:
			url = "%s&passwd=%s"% (url, password)
		r = self.session.get(url)
		result = dudesnude.parsers.profile(r.text)
		return result


class Client(CommonClient):
	def __init__(self, email, password, cookies=None):
		""" Creates a client with the given email and password
		    An optional CookieJar can be given to enable disk persistence
		"""
		if cookies is None:
			self.cookies = CookieJar()
			self.session = requests.Session()
			self.session.cookies = self.cookies
			logged_in = False
		else:
			self.cookies = cookies
			try:
				self.cookies.load()
			except:
				pass
			self.session = requests.Session()
			self.session.cookies = self.cookies
			logged_in = self.check_login()
		if not logged_in:
			self.connect(email, password)

	def check_login(self):
		r = self.session.get("https://dudesnude.com/mainpage.php")
		if r.url == "https://dudesnude.com/enable_cookies.php":
			return False
		else:
			return True

	def connect(self, email, password):
		r = self.session.get("https://dudesnude.com/login.php")
		find_s_token = re.search(r'<input.*name="s_token".*value="([^"]*)"', r.text)
		ez_cookies = requests.utils.dict_from_cookiejar(self.cookies)
		login_data = {
			'PHPSESSID': ez_cookies['PHPSESSID'],
			'email': email,
			'pass': password,
			'logon': '1',
			'seeking': '0',
			'show_online': '0',
			'tz': '0',	# pretend to be in GMT
			'win_x': '1080',
			'win_y': '720'
		}
		if find_s_token:
			login_data['s_token'] = find_s_token.group(1)
		else:
			raise Exception('Could not find login security token')

		r = self.session.post("https://dudesnude.com/login.php", data=login_data)
		if r.url == 'https://dudesnude.com/login.php':
			raise Exception('Failure to log in')
		try:
			self.cookies.save(ignore_discard=True)
		except (AttributeError, NotImplementedError):
			pass

	def get_messages_folder(self, folder=1, page_number=1):
		r = self.session.get("https://dudesnude.com/mainpage.php?msgindex=%s&folder=%s&msg_order=2" % ((page_number-1)*10, folder))
		return dudesnude.parsers.message_list(r.text)

	def get_inbox(self, page_number=1):
		return self.get_messages_folder(folder=MSGFOLDER.INBOX, page_number=page_number)

	def get_messages_saved(self, page_number=1):
		return self.get_messages_folder(folder=MSGFOLDER.SAVED, page_number=page_number)

	def get_messages_sent(self, page_number=1):
		return self.get_messages_folder(folder=MSGFOLDER.SENT, page_number=page_number)

	def get_messages_deleted(self, page_number=1):
		return self.get_messages_folder(folder=MSGFOLDER.DELETED, page_number=page_number)

	def iter_messages(self, folder=1, page_number=1):
		first = self.get_messages_folder(folder=folder, page_number=page_number)
		for message in first['messages']:
			yield message
		for page in range(page_number+1, first['num_pages']+1):
			second = self.get_messages_folder(folder, page)
			for message in second['messages']:
				yield message

	def get_message_history(self, uid):
		r = self.session.get("https://dudesnude.com/msghist.php?id=%s" % (uid, ))
		return dudesnude.parsers.message_history(r.text)

	def logout(self):
		r = self.session.get("https://dudesnude.com/logout.php")


class GuestClient(CommonClient):
	def __init__(self, cookies=None):
		if cookies is None:
			self.cookies = CookieJar()
			self.session = requests.Session()
			self.session.cookies = self.cookies
			logged_in = False
		else:
			self.cookies = cookies
			try:
				self.cookies.load()
			except:
				pass
			self.session = requests.Session()
			self.session.cookies = self.cookies
			logged_in = self.check_login()
		if not logged_in:
			self.connect()

	def check_login(self):
		r = self.session.get("https://dudesnude.com/search_menu.php")
		if r.url == "https://dudesnude.com/enable_cookies.php":
			return False
		else:
			return True

	def connect(self):
		r = self.session.get("https://dudesnude.com/warning.php")
		login_data = {
			'go': '1',
			'iagree': '1',
			'token': '',
			'win_x': '1080',
			'win_y': '720'
		}
		r = self.session.get("https://dudesnude.com/warning.php", params=login_data)
		if r.url != 'https://dudesnude.com/search_menu.php':
			raise Exception('Failure to log in')
		try:
			self.cookies.save(ignore_discard=True)
		except (AttributeError, NotImplementedError):
			pass
