# coding=utf-8
from datetime import datetime
import os
import dateutil.tz
import unittest

import dudesnude.parsers

_module_path = os.path.abspath(__file__)
_tests_path = os.path.dirname(_module_path)
_html_path = os.path.join(_tests_path, 'html')

class TestOverview(unittest.TestCase):
	def testParseEmpty(self):
		pathname = os.path.join(_html_path, 'sent1.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		parsed = dudesnude.parsers.message_list(html)
		messages = parsed['messages']
		self.assertEqual(4, len(messages))
		self.assertEqual(1, parsed['cur_page'])
		self.assertEqual(3, parsed['num_pages'])
		self.assertEqual('Looking great :)', messages[0]['summary'])
		self.assertEqual('000123', messages[0]['userid'])
		self.assertEqual('username', messages[0]['username'])
		self.assertEqual('You look amazing! How was your week? :)', messages[1]['summary'])
		self.assertEqual('Looking great!', messages[2]['summary'])
		self.assertEqual('What video games do you like?', messages[3]['summary'])
