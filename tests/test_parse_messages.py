# coding=utf-8
from datetime import datetime
import os
import dateutil.tz
import unittest

import dudesnude.parsers

_module_path = os.path.abspath(__file__)
_tests_path = os.path.dirname(_module_path)
_html_path = os.path.join(_tests_path, 'html')

class TestOverview(unittest.TestCase):
	def testParse(self):
		pathname = os.path.join(_html_path, 'inbox1.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		parsed = dudesnude.parsers.message_list(html)
		messages = parsed['messages']
		self.assertEqual(10, len(messages))

		self.assertEqual('351050757', messages[0]['messageid'])
		self.assertEqual('100007', messages[0]['userid'])
		self.assertEqual('user1', messages[0]['username'])
		self.assertEqual(datetime(2016,1,16,12,24,0,0,dateutil.tz.tzutc()), messages[0]['timestamp'])
		self.assertEqual(u'Thank you! ☺\n\nI\'ve been doing partner acrobatics for most of my life. You should try it:...', messages[0]['summary'])
		self.assertEqual('thanx man :-)', messages[2]['summary'])
		self.assertEqual('new', messages[0]['state'])
		self.assertEqual('read', messages[1]['state'])
		self.assertEqual('deleted', messages[2]['state'])
		self.assertEqual('replied', messages[3]['state'])
		self.assertEqual('unread', messages[6]['state'])

		self.assertEqual('longmember', messages[8]['username'])
		self.assertEqual('longmemberid', messages[8]['userid'])

		self.assertEqual('thanks cocksucker', messages[8]['summary'])

		self.assertEqual(39, parsed['num_pages'])
		self.assertEqual(2, parsed['cur_page'])
