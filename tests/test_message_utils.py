import base64
from datetime import datetime
import dateutil
import httpretty
import os
import re
import unittest

import dudesnude.parsers
import dudesnude.message_utils

_module_path = os.path.abspath(__file__)
_tests_path = os.path.dirname(_module_path)
_html_path = os.path.join(_tests_path, 'html')


def mock_server(request, uri, headers):
	return (200,
		{},
		uri.encode('utf-8')
	)


class TestClient(object):
	def iter_messages(self, folder=1):
		# first message in inbox
		message1 = {
			'messageid': '35101',
			'userid': '10007',
			'username': 'user1',
			'timestamp': datetime(2016,1,16,12,24,0,0,dateutil.tz.tzutc()),
			'summary': 'thanks man :-)',
			'state': 'new'
		}
		# second message in inbox
		message2 = {
			'messageid': '35102',
			'userid': '10007',
			'username': '10007',
			'timestamp': datetime(2016,1,16,18,55,0,0,dateutil.tz.tzutc()),
			'summary': 'how are you?',
			'state': 'new'
		}
		# message in sent box
		message3 = {
			'messageid': '35103',
			'userid': '10007',
			'username': '10007',
			'timestamp': datetime(2016,1,16,12,49,0,0,dateutil.tz.tzutc()),
			'summary': 'sent',
			'state': 'new'
		}
		if folder == 1:
			yield message1
			yield message1	# next page shows same message, perhaps
			yield message2
		if folder == 3:
			yield message3

	def get_message_history(self, userid):
		# matches first message in inbox
		message1 = {
			'from': userid,
			'to': 'You',
			'timestamp': datetime(2016,1,16,12,24,0,0,dateutil.tz.tzutc()),
			'text': 'thanks man :-)',
			'pictures': [{
				'preview': 'http://fs15.dudesnude.com/pic_t/3/39/pic1.jpg',
				'full': 'http://fs15.dudesnude.com/pic/3/39/pic1.jpg'
			}]
		}
		# sent, but no longer shows up in sentbox
		message2 = {
			'from': 'You',
			'to': userid,
			'timestamp': datetime(2016,1,17,12,44,0,0,dateutil.tz.tzutc()),
			'text': 'deleted sent',
			'pictures': [{
				'preview': 'http://fs15.dudesnude.com/pic_t/3/39/pic1.jpg',
				'full': 'http://fs15.dudesnude.com/pic/3/39/pic1.jpg'
			}]
		}
		# sent, shows up in sentbox
		message3 = {
			'from': 'You',
			'to': userid,
			'timestamp': datetime(2016,1,17,12,49,0,0,dateutil.tz.tzutc()),
			'text': 'sent'
		}
		return [message1, message2]

class TestEmailMessage(unittest.TestCase):
	def testConvert(self):
		pathname = os.path.join(_html_path, 'history1.html')
		with open(pathname, 'r') as data:
			html = data.read()
		raw_messages = dudesnude.parsers.message_history(html)
		self.assertEqual(3, len(raw_messages))

		# test simple
		rfc = dudesnude.message_utils.convert_message(raw_messages[0])
		rfc.as_string()	# add any mimetype stuffs
		self.assertEqual('You <You@dudesnude.com>', rfc['From'])
		self.assertEqual('username <username@dudesnude.com>', rfc['To'])
		self.assertEqual('Sat, 16 Jan 2016 12:02:00 -0000', rfc['Date'])
		self.assertFalse(rfc.is_multipart())
		self.assertEqual('text/plain; charset="utf-8"', rfc['Content-Type'])
		self.assertEqual('8bit', rfc['Content-Transfer-Encoding'])

		# test attachments
		rfc = dudesnude.message_utils.convert_message(raw_messages[1])
		rfc.as_string()	# add any mimetype stuffs
		self.assertEqual('username <username@dudesnude.com>', rfc['From'])
		self.assertEqual('You <You@dudesnude.com>', rfc['To'])
		self.assertEqual('Sat, 16 Jan 2016 12:24:00 -0000', rfc['Date'])
		self.assertTrue(rfc.is_multipart())
		self.assertEqual('multipart/mixed', rfc['Content-Type'].split(';')[0])
		parts = rfc.get_payload()
		self.assertEqual('text/plain; charset="utf-8"', parts[0]['Content-Type'])
		self.assertEqual('7bit', parts[0]['Content-Transfer-Encoding'])
		self.assertEqual('image/jpeg', parts[1]['Content-Type'])
		self.assertEqual('attachment; filename=pic1.jpg; url=http://fs15.dudesnude.com/pic/3/39/pic1.jpg', parts[1]['Content-Disposition'])
		self.assertEqual('', parts[1].get_payload())

		# try downloading an attachment
		httpretty.enable()
		httpretty.register_uri(httpretty.GET, re.compile('.*'), body=mock_server)
		self.assertEqual('7bit', parts[1]['Content-Transfer-Encoding'])
		dudesnude.message_utils.attach_pictures(rfc)
		httpretty.disable()
		httpretty.reset()
		self.assertEqual('base64', parts[1]['Content-Transfer-Encoding'])
		att = base64.b64encode('http://fs15.dudesnude.com/pic/3/39/pic1.jpg'.encode('utf-8'))
		att = att.decode('utf-8')
		self.assertEqual(att.strip(), parts[1].get_payload().strip())

	def test_download_all(self):
		client = TestClient()
		fetcher = dudesnude.message_utils.Fetcher(client)
		inbox_messages = list(fetcher.fetch(1))
		sent_messages = list(fetcher.fetch(3))
		extra_messages = list(fetcher.fetch_remainder())
		# loading from inbox
		self.assertEqual(2, len(inbox_messages))
		self.assertEqual('10007', inbox_messages[0]['from'])
		self.assertEqual('You', inbox_messages[0]['to'])
		self.assertEqual(datetime(2016,1,16,12,24,0,0,dateutil.tz.tzutc()), inbox_messages[0]['timestamp'])
		self.assertEqual('10007', inbox_messages[1]['from'])
		self.assertEqual('You', inbox_messages[1]['to'])
		self.assertEqual(datetime(2016,1,16,18,55,0,0,dateutil.tz.tzutc()), inbox_messages[1]['timestamp'])
		# loading from sent
		self.assertEqual(1, len(sent_messages))
		self.assertEqual('You', sent_messages[0]['from'])
		self.assertEqual('10007', sent_messages[0]['to'])
		self.assertEqual(datetime(2016,1,16,12,49,0,0,dateutil.tz.tzutc()), sent_messages[0]['timestamp'])
		# loading from message history
		self.assertEqual(1, len(extra_messages))
		self.assertEqual('read', extra_messages[0]['state'])
		self.assertEqual('You', extra_messages[0]['from'])
		self.assertEqual('10007', extra_messages[0]['to'])
		self.assertEqual(datetime(2016,1,17,12,44,0,0,dateutil.tz.tzutc()), extra_messages[0]['timestamp'])
		rfc = dudesnude.message_utils.convert_message(inbox_messages[0])
		parts = rfc.get_payload()
		self.assertEqual('', parts[1].get_payload())
		httpretty.enable()
		httpretty.register_uri(httpretty.GET, re.compile('.*'), body=mock_server)
		self.assertEqual('7bit', parts[1]['Content-Transfer-Encoding'])
		dudesnude.message_utils.attach_pictures(rfc)
		httpretty.disable()
		httpretty.reset()
		self.assertEqual('base64', parts[1]['Content-Transfer-Encoding'])
		att = base64.b64encode('http://fs15.dudesnude.com/pic/3/39/pic1.jpg'.encode('utf-8'))
		att = att.decode('utf-8')
		self.assertEqual(att.strip(), parts[1].get_payload().strip())
