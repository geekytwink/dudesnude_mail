# coding=utf-8
from datetime import datetime
import os
import dateutil.tz
import unittest

import dudesnude.parsers

_module_path = os.path.abspath(__file__)
_tests_path = os.path.dirname(_module_path)
_html_path = os.path.join(_tests_path, 'html')

class TestProfile(unittest.TestCase):
	def testParsePassword(self):
		pathname = os.path.join(_html_path, 'profile1.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		parsed = dudesnude.parsers.profile(html)

		self.assertEqual('Username', parsed['username'])
		self.assertTrue(parsed['online'])
		self.assertEqual(22, parsed['age'])
		self.assertEqual('United States : California : Los Angeles', parsed['location'])
		self.assertEqual('012345', parsed['userid'])
		desired_infobox = {
			'profile id': '012345',
			'distance': '302 miles',
			'build': 'Defined',
			'height': '1.70m (5\' 7")',
			'weight': '61Kg (135lb)',
			'waist': '70cm (28")',
			'chest': '102cm (42")',
			'body hair': 'Naturally Smooth',
			'facial hair': 'Stubble',
			'ethnicity': 'Caucasian',
			'cock size': 'Large',
			'his cock is': 'Cut',
			'piercings': 'Ears.',
			'tattoos': 'Several',
			'preferred role': 'Top',
			'interests': 'Underwear, Muscle worship, Email/chat, Sportswear.',
			'practice safer sex': 'Always',
			'last here': '05 Mar 2016'
		}
		self.assertEqual(desired_infobox, parsed['infobox'])

		pics = parsed['pictures']
		self.assertEqual(5, len(pics))
		self.assertEqual('http://fs15.dudesnude.com/pic_t/6/10/0123_234.jpg', pics[0]['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/6/10/0123_234.jpg', pics[0]['full'])
		self.assertFalse(pics[0]['private'])
		self.assertFalse('comment' in pics[0])
		self.assertEqual('http://fs15.dudesnude.com/pic_t/0/12/4235_823.jpg', pics[2]['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/0/12/4235_823.jpg', pics[2]['full'])
		self.assertFalse(pics[2]['private'])
		self.assertEqual('Run to the sign ;D', pics[2]['comment'])
		self.assertEqual('http://fs15.dudesnude.com/pic_t/8/37/987_234.jpg', pics[3]['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/8/37/987_234.jpg', pics[3]['full'])
		self.assertTrue(pics[3]['private'])
		self.assertEqual('Morning stroke', pics[3]['comment'])
		self.assertEqual('http://fs15.dudesnude.com/pic_t/0/4/9458_234.jpg', pics[4]['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/0/4/9458_234.jpg', pics[4]['full'])
		self.assertTrue(pics[4]['private'])
		self.assertFalse('comment' in pics[4])

		vids = parsed['videos']
		self.assertEqual(2, len(vids))
		self.assertFalse(vids[0]['private'])
		self.assertEqual('11sec', vids[0]['length'])
		self.assertEqual('251Kb', vids[0]['size'])
		self.assertEqual('http://dudesnude.com/vid/9/02314-42/42.mp4', vids[0]['full'])
		self.assertEqual('http://dudesnude.com/view_video.php?vid=9/02314-42', vids[0]['view_link'])
		self.assertEqual('http://dudesnude.com/view_video.php?vid=9%2F02314-42&download=1', vids[0]['download_link'])
		self.assertEqual(5, len(vids[0]['previews']))
		self.assertTrue(vids[1]['private'])
		self.assertEqual('15sec', vids[1]['length'])
		self.assertEqual('851Kb', vids[1]['size'])
		self.assertEqual('http://dudesnude.com/vid/9/02314-43/43.mp4', vids[1]['full'])
		self.assertEqual('http://dudesnude.com/view_video.php?vid=9/02314-43', vids[1]['view_link'])
		self.assertEqual('http://dudesnude.com/view_video.php?vid=9%2F02314-43&download=1', vids[1]['download_link'])
		self.assertEqual(5, len(vids[1]['previews']))
		self.assertEqual("Hey guys, here's the profile description\nI'm really a good guy, let's have fun :)", parsed['text'])

	def testParsePlain(self):
		pathname = os.path.join(_html_path, 'profile2.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		parsed = dudesnude.parsers.profile(html)

		self.assertEqual('users', parsed['username'])
		self.assertTrue(parsed['online'])
		self.assertFalse('age' in parsed)
		self.assertEqual('United States : California : Los Altos', parsed['location'])
		self.assertEqual('987', parsed['userid'])
		desired_infobox = {
			'profile id': '987',
			'distance': '10 miles',
			'build': 'Slim',
			'body hair': 'Naturally Smooth',
			'his cock is': 'Uncut',
			'interests': 'Underwear, Muscle worship, Email/chat, Fetish, Sportswear.',
			'practice safer sex': 'Always',
			'last here': '07 Mar 2016'
		}
		self.assertEqual(desired_infobox, parsed['infobox'])

		pics = parsed['pictures']
		self.assertEqual(1, len(pics))
		self.assertEqual('http://fs15.dudesnude.com/pic_t/0/15/234_987.jpg', pics[0]['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/0/15/234_987.jpg', pics[0]['full'])
		self.assertFalse(pics[0]['private'])
		self.assertEqual('body', pics[0]['comment'])
		self.assertEqual("", parsed['text'])

	def testParse3(self):
		pathname = os.path.join(_html_path, 'profile3.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		parsed = dudesnude.parsers.profile(html)

		self.assertEqual('bigstud', parsed['username'])
		self.assertTrue(parsed['online'])
		self.assertTrue(parsed['verified'])
		self.assertTrue(parsed['supporter'])
		self.assertFalse('age' in parsed)
		self.assertEqual('U.S.A. : California : San Jose', parsed['location'])
		self.assertEqual('0000234', parsed['userid'])
		desired_infobox = {
			'profile id': '0000234',
			'distance': '11 miles',
			'build': 'Muscular',
			'height': '1.78m (5\' 10")',
			'weight': '87Kg (192lb)',
			'waist': '84cm (33")',
			'chest': '107cm (42")',
			'biceps': '41cm (16")',
			'body hair': 'Shaved',
			'facial hair': 'Clean-Shaven',
			'ethnicity': 'Caucasian',
			'tattoos': 'None',
			'interests': 'Muscle worship, Email/chat, Fetish.',
			'last here': '15 Mar 2016'
		}
		self.assertEqual(desired_infobox, parsed['infobox'])

		desired_links = [{
			'name': 'My Twitter',
			'href': 'http://www.twitter.com/bigstud2005',
			'icon': 'http://dudesnude.com/static/icons/link_icon2.png'
		},{
			'name': 'My Homepage',
			'href': 'http://instagram.com/bigstude#',
			'icon': 'http://dudesnude.com/static/icons/link_icon255.png'
		}]
		self.assertEqual(desired_links, parsed['links'])

		pics = parsed['pictures']
		self.assertEqual(17, len(pics))
		self.assertEqual('http://fs15.dudesnude.com/pic_t/9/43/000_2345.jpg', pics[0]['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/9/43/000_2345.jpg', pics[0]['full'])
		self.assertFalse(pics[0]['private'])
		self.assertEqual("not looking for anything in particular but always down for a hot body", parsed['text'])
