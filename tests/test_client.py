from datetime import datetime
import dateutil
import httpretty
import itertools
import os
import re
import requests
import unittest
import uuid

from six.moves.urllib_parse import urlparse
from six.moves.urllib_parse import parse_qs
from six.moves.http_cookiejar import CookieJar
from six.moves.http_cookies import SimpleCookie

import dudesnude.client

_module_path = os.path.abspath(__file__)
_tests_path = os.path.dirname(_module_path)
_html_path = os.path.join(_tests_path, 'html')


# disable connection pooling because httpretty
Pool = requests.packages.urllib3.connectionpool.HTTPConnectionPool
Pool._get_conn = lambda self, **kwargs: self._new_conn()


# set up fake web server
mock_sessions = {}
mock_allow_guest = True

def mock_server(request, uri, headers):
	# url params parsing
	urlparsed = urlparse(uri)
	params = parse_qs(urlparsed.query)
	# session handling
	sessid = None
	cookie = SimpleCookie()
	cookie.load(request.headers.get('Cookie', ''))
	if 'PHPSESSID' in cookie:
		sessid = cookie['PHPSESSID'].value
	else:
		sessid = uuid.uuid4()

	# static server
	if 'fs' in urlparsed.netloc:
		return (200, {}, uri.encode('utf-8'))

	# url routing
	if uri.endswith('/enable_cookies.php'):
		# no session
		return (
			200,
			{
				'Set-Cookie': 'PHPSESSID=%s;' % (sessid,)
			},
			'Please login'.encode('utf-8')
		)
	if uri.endswith('/login.php'):
		# user login
		if request.method == 'GET':
			return (
				200,
				{
					'Set-Cookie': 'PHPSESSID=%s;' % (sessid,)
				},
				'Please login'.encode('utf-8') +
				'<input name="s_token" value="test" />'.encode('utf-8')
			)
		args = parse_qs(request.body)
		decode_args = dict([
			(k.decode('utf-8'), v[0].decode('utf-8'))
			for (k,v) in args.items()
		])
		if decode_args.get('email') == 'user@name' and \
		   decode_args.get('pass') == 'password':
			mock_sessions[sessid] = True
			return (
				302,
				{
					'Location': 'https://dudesnude.com/mainpage.php'
				},
				'Success'.encode('utf-8')
			)
		else:
			return (
				200,
				{},
				'Invalid'.encode('utf-8')
			)
	if '/warning.php' in uri:
		# guest login
		if mock_allow_guest and \
		   request.method == 'GET' and \
		   params.get('go', ['0'])[0] == '1':
			# successful guest click
			mock_sessions[sessid] = True
			return (
				302,
				{
					'Location': 'https://dudesnude.com/search_menu.php'
				},
				'Success'.encode('utf-8')
			)
		else:
			return (
				200,
				{
					'Set-Cookie': 'PHPSESSID=%s;' % (sessid,)
				},
				'Please login'.encode('utf-8')
			)

	# unauthenticated requests
	if not mock_sessions.get(sessid, False):
		# redirect to login page
		return (
			302,
			{
				'Location': 'https://dudesnude.com/enable_cookies.php',
				'Set-Cookie': 'PHPSESSID=%s;' % (sessid,)
			},
			''.encode('utf-8')
		)

	# needs authentication to continue
	if '/mainpage.php' in uri:
		# return mailbox
		pathname = os.path.join(_html_path, 'inbox1.html')
		with open(pathname, 'rb') as data:
			html = data.read()
		return (200, {}, html)
	if '/msghist.php' in uri:
		# return message history
		pathname = os.path.join(_html_path, 'history1.html')
		with open(pathname, 'rb') as data:
			html = data.read()
		return (200, {}, html)
	if '/runsearch.php' in uri:
		# return search result
		pathname = os.path.join(_html_path, 'search1.html')
		with open(pathname, 'rb') as data:
			html = data.read()
		return (200, {}, html)
	if '/show.php' in uri:
		# return search result
		if 'id=1' in uri and 'passwd=yes' in uri:
			pathname = os.path.join(_html_path, 'profile1.html')
		else:
			pathname = os.path.join(_html_path, 'profile2.html')
		with open(pathname, 'rb') as data:
			html = data.read()
		return (200, {}, html)
	if '/logout.php' in uri:
		# logout
		del mock_sessions[sessid]
		return (200, {}, 'Logged out')

	# unknown thing
	print("Unhandled mock uri: %s" % (uri, ))
	return (404, {}, 'Missing')

class TestClient(unittest.TestCase):
	def setUp(self):
		global mock_allow_guest
		mock_sessions.clear()
		mock_allow_guest = True
		httpretty.enable()
		httpretty.register_uri(httpretty.GET, re.compile('.*'), body=mock_server)
		httpretty.register_uri(httpretty.POST, re.compile('.*'), body=mock_server)
	def tearDown(self):
		httpretty.disable()

	# test logins
	def test_login(self):
		client = dudesnude.client.Client('user@name', 'password')
		self.assertTrue(client.check_login())
	def test_failed_login(self):
		self.assertRaises(Exception, dudesnude.client.Client, 'user@name', 'wrong')
	def test_reuse_session(self):
		cookies = CookieJar()
		client = dudesnude.client.Client('user@name', 'password', cookies)
		# should reuse cookie session
		cached_client = dudesnude.client.Client('user@name', '', cookies)
	def test_reuse_session_timeout(self):
		cookies = CookieJar()
		client = dudesnude.client.Client('user@name', 'password', cookies)
		# session timed out
		mock_sessions.clear()
		self.assertRaises(Exception, dudesnude.client.Client, 'user@name', '', cookies)
	def test_guest_login(self):
		client = dudesnude.client.GuestClient()
	def test_guest_login_failed(self):
		global mock_allow_guest
		mock_allow_guest = False
		self.assertRaises(Exception, dudesnude.client.GuestClient)
	def test_guest_cached(self):
		cookies = CookieJar()
		client = dudesnude.client.GuestClient(cookies)
		cached_client = dudesnude.client.GuestClient(cookies)
	def test_guest_cached_timeout(self):
		cookies = CookieJar()
		client = dudesnude.client.GuestClient(cookies)
		# session timed out
		mock_sessions.clear()
		cached_client = dudesnude.client.GuestClient(cookies)
	def test_guest_cached_invalid(self):
		cookies = CookieJar()
		client = dudesnude.client.GuestClient(cookies)
		global mock_allow_guest
		mock_allow_guest = False
		cached_client = dudesnude.client.GuestClient(cookies)
	def test_guest_cached_timeout(self):
		cookies = CookieJar()
		client = dudesnude.client.GuestClient(cookies)
		# session timed out
		mock_sessions.clear()
		global mock_allow_guest
		mock_allow_guest = False
		self.assertRaises(Exception, dudesnude.client.GuestClient, cookies)
	# test searches
	def test_guest_search(self):
		client = dudesnude.client.GuestClient()
		result = client.is_online('UserName')
		self.assertTrue(result)
	def test_loggedin_search(self):
		client = dudesnude.client.Client('user@name', 'password')
		result = client.is_online('UserName')
		self.assertTrue(result)
	def test_guest_uid(self):
		client = dudesnude.client.GuestClient()
		result = client.username2uid('UserName')
		self.assertEquals('1000009', result)
	def test_loggedin_uid(self):
		client = dudesnude.client.Client('user@name', 'password')
		result = client.username2uid('UserName')
		self.assertEquals('1000009', result)

	def test_inbox(self):
		client = dudesnude.client.Client('user@name', 'password')
		messages = client.get_inbox()
		self.assertEqual(10, len(messages['messages']))
	def test_saved(self):
		client = dudesnude.client.Client('user@name', 'password')
		messages = client.get_messages_saved()
		self.assertEqual(10, len(messages['messages']))
	def test_sent(self):
		client = dudesnude.client.Client('user@name', 'password')
		messages = client.get_messages_sent()
		self.assertEqual(10, len(messages['messages']))
	def test_deleted(self):
		client = dudesnude.client.Client('user@name', 'password')
		messages = client.get_messages_deleted()
		self.assertEqual(10, len(messages['messages']))
	def test_iter(self):
		# automatically goes to next page
		client = dudesnude.client.Client('user@name', 'password')
		messages = client.iter_messages()
		self.assertTrue(hasattr(messages, '__iter__'))
		messages = list(itertools.islice(messages, 20))
		self.assertEqual(20, len(messages))
	def test_message_history(self):
		client = dudesnude.client.Client('user@name', 'password')
		messages = client.get_message_history(100007)
		self.assertEqual(3, len(messages))
	def test_profile(self):
		client = dudesnude.client.Client('user@name', 'password')
		profile = client.get_profile('1')
		self.assertEqual('987', profile['userid'])
		profile = client.get_profile('1', 'yes')
		self.assertEqual('012345', profile['userid'])
	def test_logout(self):
		client = dudesnude.client.Client('user@name', 'password')
		self.assertTrue(client.check_login())
		client.logout()
		self.assertFalse(client.check_login())
