import datetime
import mailbox
import os.path
import shutil
import tempfile
import unittest

import dudesnude.maildir_sync


class TestFolders(unittest.TestCase):
	def setUp(self):
		self.dirname = tempfile.mkdtemp()
	def tearDown(self):
		shutil.rmtree(self.dirname)

	def test_topfolder(self):
		syncer = dudesnude.maildir_sync.MaildirSync(None, self.dirname, '')
		root = mailbox.Maildir(self.dirname, factory=None)
		folders = root.list_folders()
		self.assertEqual(set(('Inbox', 'Saved', 'Sent')), set(folders))

	def test_get_folder(self):
		syncer = dudesnude.maildir_sync.MaildirSync(None, self.dirname, '')
		root = mailbox.Maildir(self.dirname, factory=None)
		folders = root.list_folders()
		self.assertEqual(set(('Inbox', 'Saved', 'Sent')), set(folders))
		# delete a folder
		shutil.rmtree(os.path.join(self.dirname, '.Sent'))
		root = mailbox.Maildir(self.dirname, factory=None)
		folders = root.list_folders()
		self.assertEqual(set(('Inbox', 'Saved')), set(folders))
		# check that it comes back
		syncer = dudesnude.maildir_sync.MaildirSync(None, self.dirname, '')
		root = mailbox.Maildir(self.dirname, factory=None)
		folders = root.list_folders()
		self.assertEqual(set(('Inbox', 'Saved', 'Sent')), set(folders))

	def test_nestedfolder(self):
		# try to create in a nested folder
		syncer = dudesnude.maildir_sync.MaildirSync(None, self.dirname, 'Nested')
		root = mailbox.Maildir(self.dirname, factory=None)
		folders = root.list_folders()
		self.assertEqual(set(('Nested',)), set(folders))
		folder = root.get_folder('Nested')
		folders = folder.list_folders()
		self.assertEqual(set(('Inbox', 'Saved', 'Sent')), set(folders))
		# try to load a nested folder
		syncer = dudesnude.maildir_sync.MaildirSync(None, self.dirname, 'Nested')
		root = mailbox.Maildir(self.dirname, factory=None)
		folders = root.list_folders()
		self.assertEqual(set(('Nested',)), set(folders))
		folder = root.get_folder('Nested')
		folders = folder.list_folders()
		self.assertEqual(set(('Inbox', 'Saved', 'Sent')), set(folders))


class FakeClient(object):
	def __init__(self):
		self.mailboxes = [
			[],	# mailbox=0?
			[],	# mailbox=1=inbox
			[],	# mailbox=2=saved
			[],	# mailbox=3=sent
			[],	# mailbox=4=deleted
		]
		self.histories = {}

	def iter_messages(self, folder=1):
		return (message for message in self.mailboxes[folder])

	def get_message_history(self, uid):
		return self.histories.get(uid, [])

	def add_message(self, folder, person, datetime, state, body, index=0):
		message = {
			'username': person,
			'timestamp': datetime,
			'state': state,
			'userid': person,
			'summary': body,
			'text': body
		}
		if folder == 3:		# sent
			message['from'] = 'You'
			message['to'] = person
		else:
			message['from'] = person
			message['to'] = 'You'
		target = self.mailboxes[folder]
		if index < 0:
			index = len(target) + index + 1
		target.insert(index, message)
		self.histories.setdefault(person, []).append(message)


class TestDownload(unittest.TestCase):
	def setUp(self):
		self.dirname = tempfile.mkdtemp()
		self.client = FakeClient()
		self.syncer = dudesnude.maildir_sync.MaildirSync(self.client, self.dirname, '')
		self.maildir = mailbox.Maildir(self.dirname, factory=None)
	def tearDown(self):
		shutil.rmtree(self.dirname)

	def test_sync_inbox(self):
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,1), 'read', 'Read message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,2), 'unread', 'Unread message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,3), 'new', 'New message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(3, len(messages))
		self.assertEqual('S', messages[0].get_flags())
		self.assertEqual('', messages[1].get_flags())
		self.assertEqual('', messages[2].get_flags())
		self.assertTrue(messages[0]['Date'].startswith('Thu, 01 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[2]['Date'].startswith('Sat, 03 Jan 2015'))

	def test_sync_inbox_duplicates(self):
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,3), 'new', 'New message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,3), 'unread', 'New message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,2), 'unread', 'Unread message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))

	def test_sync_inbox_read(self):
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,2), 'new', 'New message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,3), 'read', 'New message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertEqual('', messages[0].get_flags())
		self.assertEqual('S', messages[1].get_flags())
		# round two
		self.client.mailboxes[1][1]['state'] = 'read'
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,4), 'read', 'Newer message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(3, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertTrue(messages[2]['Date'].startswith('Sun, 04 Jan 2015'))
		self.assertEqual('S', messages[0].get_flags())
		self.assertEqual('S', messages[1].get_flags())
		self.assertEqual('S', messages[2].get_flags())

	def test_sync_inbox_optimization(self):
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,2), 'new', 'New message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,3), 'read', 'New message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertEqual('', messages[0].get_flags())
		self.assertEqual('S', messages[1].get_flags())
		# round two
		self.client.mailboxes[1][1]['state'] = 'read'
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,1), 'read', 'Invisible', index=-1)
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertEqual('S', messages[0].get_flags())
		self.assertEqual('S', messages[1].get_flags())

	def test_sync_inbox_optimization_read(self):
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,2), 'read', 'New message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,3), 'read', 'New message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))
		# round two
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,1), 'read', 'Invisible', index=-1)
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))

	def test_sync_inbox_optimization_read_deleted(self):
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,2), 'read', 'New message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,3), 'read', 'New message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))
		# round two
		self.client.mailboxes[1].pop(1)
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,1), 'read', 'Invisible', index=-1)
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))

	def test_sync_inbox_optimization_unread_deleted(self):
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,2), 'unread', 'New message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,3), 'read', 'New message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))
		# round two
		self.client.mailboxes[1].pop(1)
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,1), 'read', 'Invisible', index=-1)
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(2, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))

	def test_sync_inbox_optimization_extreme(self):
		""" Test that, even though we have more messages than we store
		    in messages.last_messages, it still detects when we get to
		    the start of messages we've already downloaded
		"""
		for day in range(3,20):
			self.client.add_message(1, 'Username', datetime.datetime(2015,1,day), 'read', 'New message')
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(17, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertTrue(messages[7]['Date'].startswith('Sat, 10 Jan 2015'))
		self.assertTrue(messages[14]['Date'].startswith('Sat, 17 Jan 2015'))
		# round two
		for index in range(0, 13):
			self.client.mailboxes[1].pop(0)
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,1), 'read', 'Invisible', index=-1)
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(17, len(messages))	# didn't find invisible message
		self.assertTrue(messages[0]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertTrue(messages[7]['Date'].startswith('Sat, 10 Jan 2015'))
		self.assertTrue(messages[14]['Date'].startswith('Sat, 17 Jan 2015'))

	def test_sync_all(self):
		self.client.add_message(1, 'username', datetime.datetime(2015,1,2), 'unread', 'new message')
		self.client.add_message(3, 'username', datetime.datetime(2015,1,3), 'read', 'new message')
		self.client.add_message(3, 'username', datetime.datetime(2015,1,4), 'read', 'deleted message')
		self.client.add_message(3, 'username', datetime.datetime(2015,1,1), 'read', 'history message')
		self.client.mailboxes[3].pop(0)   # delete 2015-1-1 from sent, keep in history
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer.sync()
		# check
		inbox = self.maildir.get_folder('Inbox')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(1, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertEqual('', messages[0].get_flags())
		# check sent
		sent = self.maildir.get_folder('Sent')
		messages = list(sent.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(3, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Thu, 01 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertTrue(messages[2]['Date'].startswith('Sun, 04 Jan 2015'))
		self.assertEqual('S', messages[0].get_flags())
		self.assertEqual('S', messages[1].get_flags())
		self.assertEqual('S', messages[2].get_flags())

	def test_sync_sent_extra(self):
		self.client.add_message(3, 'Username', datetime.datetime(2015,1,1), 'read', 'Read message')
		self.client.add_message(3, 'Username', datetime.datetime(2015,1,2), 'unread', 'Unread message')
		self.client.add_message(3, 'Username', datetime.datetime(2015,1,3), 'read', 'Archived message')
		self.client.add_message(1, 'Username', datetime.datetime(2015,1,4), 'read', 'Inbox? Archived message')
		self.client.mailboxes[3].pop(0)	# delete (2015-1-3) from sent box, still in history
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Sent')
		self.syncer._sync_extra_sent()
		inbox = self.maildir.get_folder('Sent')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(3, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Thu, 01 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[2]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertEqual('S', messages[0].get_flags())
		self.assertEqual('', messages[1].get_flags())
		self.assertEqual('S', messages[2].get_flags())
		# days pass, messages are pruned from sent
		self.client.mailboxes[3].pop(0)	# delete (2015-1-2) from sent box, still in history
		self.syncer.fetcher = dudesnude.message_utils.Fetcher(self.client)
		self.syncer._sync_section('Sent')
		self.syncer._sync_extra_sent()
		inbox = self.maildir.get_folder('Sent')
		messages = list(inbox.values())
		messages.sort(key=lambda i:i['Message-Id'])
		self.assertEqual(3, len(messages))
		self.assertTrue(messages[0]['Date'].startswith('Thu, 01 Jan 2015'))
		self.assertTrue(messages[1]['Date'].startswith('Fri, 02 Jan 2015'))
		self.assertTrue(messages[2]['Date'].startswith('Sat, 03 Jan 2015'))
		self.assertEqual('S', messages[0].get_flags())
		self.assertEqual('S', messages[1].get_flags())
		self.assertEqual('S', messages[2].get_flags())
