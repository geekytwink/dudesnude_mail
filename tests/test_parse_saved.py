# coding=utf-8
from datetime import datetime
import os
import dateutil.tz
import unittest

import dudesnude.parsers

_module_path = os.path.abspath(__file__)
_tests_path = os.path.dirname(_module_path)
_html_path = os.path.join(_tests_path, 'html')

class TestOverview(unittest.TestCase):
	def testParseEmpty(self):
		pathname = os.path.join(_html_path, 'saved1.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		parsed = dudesnude.parsers.message_list(html)
		messages = parsed['messages']
		self.assertEqual(0, len(messages))
		self.assertEqual(1, parsed['cur_page'])
		self.assertEqual(1, parsed['num_pages'])
	def testParseSaved(self):
		pathname = os.path.join(_html_path, 'saved2.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		parsed = dudesnude.parsers.message_list(html)
		messages = parsed['messages']
		self.assertEqual(1, len(messages))
		self.assertEqual(1, parsed['cur_page'])
		self.assertEqual(1, parsed['num_pages'])
		self.assertEqual('friend', messages[0]['username'])
