# coding=utf-8
from datetime import datetime
import dateutil
import os
import unittest

import dudesnude.parsers

_module_path = os.path.abspath(__file__)
_tests_path = os.path.dirname(_module_path)
_html_path = os.path.join(_tests_path, 'html')

class TestHistory(unittest.TestCase):
	def testParse(self):
		pathname = os.path.join(_html_path, 'history1.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		messages = dudesnude.parsers.message_history(html)
		self.assertEqual(3, len(messages))

		self.assertEqual('You', messages[0]['from'])
		self.assertEqual('username', messages[0]['to'])
		self.assertEqual('username', messages[1]['from'])
		self.assertEqual('You', messages[1]['to'])
		self.assertEqual('You', messages[2]['from'])
		self.assertEqual('username', messages[2]['to'])
		self.assertEqual(datetime(2016,1,16,12, 2,0,0,dateutil.tz.tzutc()), messages[0]['timestamp'])
		self.assertEqual(datetime(2016,1,16,12,24,0,0,dateutil.tz.tzutc()), messages[1]['timestamp'])
		self.assertEqual(datetime(2016,1,16,13,24,0,0,dateutil.tz.tzutc()), messages[2]['timestamp'])
		self.assertEqual('privatepass', messages[0]['password'])
		self.assertEqual('anotherpass', messages[1]['password'])
		self.assertFalse('password' in messages[2])
		self.assertFalse('email' in messages[0])
		self.assertEqual('username@email.com', messages[1]['email'])
		self.assertFalse('email' in messages[2])
		self.assertFalse('pictures' in messages[0])
		self.assertEqual(2, len(messages[1]['pictures']))
		self.assertEqual('http://fs15.dudesnude.com/pic_t/3/39/pic1.jpg', messages[1]['pictures'][0]['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/3/39/pic1.jpg', messages[1]['pictures'][0]['full'])
		self.assertEqual('http://fs15.dudesnude.com/pic_t/9/80/pic2.jpg', messages[1]['pictures'][1]['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/9/80/pic2.jpg', messages[1]['pictures'][1]['full'])
		self.assertFalse('pictures' in messages[2])
		self.assertEqual(u'Looking great :D ☺ I\'ve been taking a handstand class and I\'m slowly getting good form!', messages[0]['text'])
		self.assertEqual('Thank you!\n\nI\'ve been doing partner acrobatics for most of my life. You should try it: You get to climb on top of all those muscular guys ;-)\n\nHere\'s some test attachments', messages[1]['text'])
		self.assertEqual('You seem to be offering up something, I\'ll gladly take it :)\nPole is amazing! I get to learn fun tricks to show off on random street poles, hehe >:)', messages[2]['text'])

	def testEmpty(self):
		pathname = os.path.join(_html_path, 'history2.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		messages = dudesnude.parsers.message_history(html)
		self.assertEqual(0, len(messages))
