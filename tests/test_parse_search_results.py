# coding=utf-8
from datetime import datetime
import os
import unittest

import dudesnude.parsers

_module_path = os.path.abspath(__file__)
_tests_path = os.path.dirname(_module_path)
_html_path = os.path.join(_tests_path, 'html')

class TestSearchResults(unittest.TestCase):
	def testParse(self):
		pathname = os.path.join(_html_path, 'search1.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		results = dudesnude.parsers.search_results(html)
		self.assertEqual(1, len(results))

		self.assertEqual('1000009', results[0]['userid'])
		self.assertEqual(u'UserNȧme', results[0]['username'])
		self.assertEqual('http://fs15.dudesnude.com/pic_t/5/44/picture_name.jpg', results[0]['picture']['preview'])
		self.assertEqual('http://fs15.dudesnude.com/pic/5/44/picture_name.jpg', results[0]['picture']['full'])
		self.assertEqual(True, results[0]['online'])
	def testEmpty(self):
		pathname = os.path.join(_html_path, 'search2.html')
		with open(pathname, 'rb') as data:
			html = data.read().decode('utf-8')
		results = dudesnude.parsers.search_results(html)
		self.assertEqual(0, len(results))
