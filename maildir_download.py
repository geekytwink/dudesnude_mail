#!/usr/bin/env python
import logging
import os.path
import sys
try:
	import configparser as configparser
except:
	import ConfigParser as configparser

import dudesnude
import dudesnude.maildir_sync


logging.basicConfig(level=logging.DEBUG)


# load settings
_module_path = os.path.abspath(__file__)
_cur_path = os.path.dirname(_module_path)
_ini_path = os.path.join(_cur_path, 'settings.ini')
config = configparser.RawConfigParser()
config.read(_ini_path)


# check for the user already being online
if config.has_option('dudesnude', 'username'):
	username = config.get('dudesnude', 'username')
	guest = dudesnude.GuestClient()
	if guest.is_online(username):
		print("You appear to be logged in to the web interface, not syncing")
		sys.exit(0)

# log in
dn_email = config.get('dudesnude', 'email')
dn_password = config.get('dudesnude', 'password')
client = dudesnude.Client(dn_email, dn_password)

# create sync
maildir_path = config.get('maildir', 'path')
os.path.expanduser(maildir_path)
maildir_subdir = config.get('maildir', 'subdir')
syncer = dudesnude.maildir_sync.MaildirSync(client, maildir_path, maildir_subdir)

# sync
syncer.sync()

# logout so the site sends normal email notifications
client.logout()
